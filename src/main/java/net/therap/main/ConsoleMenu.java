package net.therap.main;

import net.therap.domain.DailyMeal;
import net.therap.domain.Item;
import net.therap.domain.MenuOption;
import net.therap.domain.Type;
import net.therap.service.MenuService;
import net.therap.utility.HibernateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author nourin
 * @since 11/22/16
 */
public class ConsoleMenu {

    private final Scanner scanner = new Scanner(System.in);
    private final MenuService menuService = new MenuService();
    private final String WEEKDAYS[] = {"Sunday", "Monday", "Tuesday", "Wednsday", "Thursday"};

    public void start() {

        int choice;
        System.out.println();
        System.out.println("\n############## MENU MANAGER #################\n");
        choice = showAndGetOption();
        performOption(getChoiceEnum(choice));
        System.out.println();
    }

    private void performOption(MenuOption choice) {

        switch (choice) {
            case ALL_MENU:
                showMenu("Breakfast");
                showMenu("Lunch");
                break;
            case BREAKFAST:
                showMenu("Breakfast");
                break;
            case LUNCH:
                showMenu("Lunch");
                break;
            case UPDATE_BREAKFAST:
                showEditMenu("Breakfast", MenuOption.BREAKFAST);
                break;
            case UPDATE_LUNCH:
                showEditMenu("Lunch", MenuOption.LUNCH);
                break;
            default:
                System.out.println("Exiting the program.....");
                HibernateUtil.closeSessionFactory();
                break;
        }

        if (!choice.equals(MenuOption.EXIT)) {
            start();
        }
    }

    private void showMenu(String typeName) {

        List<DailyMeal> dailyMealList = menuService.getDailyMealList();
        boolean first = true;

        System.out.println("\n############ " + typeName.toUpperCase() + " MENU: ############\n");

        for (DailyMeal dailyMeal : dailyMealList) {

            System.out.print(dailyMeal.getName() + " - ");
            first = true;

            for (Item item : dailyMeal.getItems()) {

                if (item.hasType(new Type(typeName)) != -1) {

                    if (first) {
                        System.out.print(item.getName());
                        first = false;
                    } else {
                        System.out.print(", " + item.getName());
                    }
                }
            }
            System.out.println();
        }
    }


    private void showEditMenu(String type, MenuOption option) {

        System.out.format("%5s\n", "1. Add to " + type + " Menu");
        System.out.format("%5s\n", "2. Update " + type + " Menu");
        System.out.format("%5s\n", "3. Delete from " + type + " Menu");

        System.out.println("Enter an integer value as your choice:");
        int choice = scanner.nextInt();
        if (choice == 1) {
            addToMenu(type, option);
        } else if (choice == 2) {
            addToMenu(type, option);
        } else if (choice == 3) {
            deleteFromMenu(type);
        }
    }

    private void deleteFromMenu(String type) {

        String choosenDay = choiceDay();
        menuService.deleteFromMenu(choosenDay, type);
    }

    private void addToMenu(String type, MenuOption menuType) {

        String choosenDay = choiceDay();
        List<Item> itemList = getItems();
        menuService.addToMenu(choosenDay, itemList, type);
    }

    private List<Item> getItems() {

        ArrayList<Item> itemList = new ArrayList<>();
        System.out.println("Enter the number of items: ");
        int itemNumber = scanner.nextInt();
        Item item;

        for (int i = 0; i < itemNumber; i++) {

            System.out.print("Item #" + (i + 1) + ": ");
            item = new Item();
            item.setName(scanner.next());
            itemList.add(item);
        }

        return itemList;
    }

    private String choiceDay() {

        System.out.println("Choice WeekDay. Enter your choice number: ");
        for (int i = 0; i < WEEKDAYS.length; i++) {
            System.out.println((i + 1) + ". " + WEEKDAYS[i]);
        }
        int choice = scanner.nextInt();

        return WEEKDAYS[choice - 1];
    }

    private int showAndGetOption() {

        System.out.format("%5s\n", "1. Show All Menu");
        System.out.format("%5s\n", "2. Show BreakFast Menu");
        System.out.format("%5s\n", "3. Show Lunch Menu");
        System.out.format("%5s\n", "4. Update BreakFast Menu");
        System.out.format("%5s\n", "5. Update Lunch Menu");
        System.out.format("%5s\n", "6. Exit Program");
        System.out.println();
        System.out.println("Enter an integer value as your choice:");

        return scanner.nextInt();
    }

    private MenuOption getChoiceEnum(int choice) {

        if (choice == MenuOption.ALL_MENU.getValue()) {

            return MenuOption.ALL_MENU;

        } else if (choice == MenuOption.BREAKFAST.getValue()) {

            return MenuOption.BREAKFAST;

        } else if (choice == MenuOption.LUNCH.getValue()) {

            return MenuOption.LUNCH;

        } else if (choice == MenuOption.UPDATE_BREAKFAST.getValue()) {

            return MenuOption.UPDATE_BREAKFAST;

        } else if (choice == MenuOption.UPDATE_LUNCH.getValue()) {

            return MenuOption.UPDATE_LUNCH;

        } else if (choice == MenuOption.EXIT.getValue()) {

            return MenuOption.EXIT;
        }

        return MenuOption.INVALID;
    }
}

