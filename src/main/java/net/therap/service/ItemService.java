package net.therap.service;

import net.therap.dao.GenericDao;
import net.therap.dao.GenericDaoImpl;
import net.therap.domain.Item;
import net.therap.domain.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/23/16
 */
public class ItemService {

    private GenericDao<Item, Integer> itemDao = new GenericDaoImpl<>(Item.class);
    private GenericDao<Type, Integer> typeDao = new GenericDaoImpl<>(Type.class);

    public List<Item> checkItems(List<Item> items, String type) {

        List<Item> checkedList = new ArrayList<>();
        Item checkedItem;

        for (Item item : items) {

            checkedItem = itemDao.findByName(item.getName());

            if (checkedItem == null) {

                checkedList.add(saveItem(item, type));
            } else {

                checkedList.add(updateItem(checkedItem, type));
            }
        }

        return checkedList;
    }

    public Item saveItem(Item item, String typeName) {

        Type type = typeDao.findByName(typeName);
        item.addType(type);
        item.setId(itemDao.save(item));

        return item;
    }

    public Item updateItem(Item item, String typeName) {

        Type type = typeDao.findByName(typeName);
        item.addType(type);
        itemDao.update(item);

        return itemDao.findById(item.getId());
    }
}
