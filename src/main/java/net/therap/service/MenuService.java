package net.therap.service;

import net.therap.dao.GenericDao;
import net.therap.dao.GenericDaoImpl;
import net.therap.domain.DailyMeal;
import net.therap.domain.Item;
import net.therap.domain.Type;

import java.util.List;

/**
 * @author nourin
 * @since 11/22/16
 */
public class MenuService {

    private final GenericDao<DailyMeal, Integer> dailyMealDao = new GenericDaoImpl<>(DailyMeal.class);
    private final GenericDao<Item, Integer> itemDao = new GenericDaoImpl<>(Item.class);
    private final ItemService itemService = new ItemService();

    public List<DailyMeal> getDailyMealList() {

        return dailyMealDao.findAll();
    }

    public void addToMenu(String day, List<Item> itemList, String type) {

        DailyMeal dailyMeal = dailyMealDao.findByName(day);
        dailyMeal.setItems(itemService.checkItems(itemList, type));
        dailyMealDao.update(dailyMeal);
    }

    public void deleteFromMenu(String day, String type) {

        DailyMeal dailyMeal = dailyMealDao.findByName(day);
        List<Item> itemList = dailyMeal.getItems();
        Type itemType = new Type(type);

        if (itemList != null) {

            for (Item item : itemList) {

                List<Type> typeList = item.getTypes();
                int typeIndex = item.hasType(itemType);

                if (typeIndex != -1) {

                    typeList.remove(typeIndex);
                    itemDao.update(item);
                    item = itemDao.findById(item.getId());
                }
            }
            dailyMeal.setItems(itemList);
            dailyMealDao.update(dailyMeal);
        }
    }

}
