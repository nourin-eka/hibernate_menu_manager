package net.therap.dao;

import net.therap.utility.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

/**
 * @author nourin
 * @since 11/22/16
 */
public class GenericDaoImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

    protected Class<T> type;

    public GenericDaoImpl(Class<T> type) {
        this.type = type;
    }

    @Override
    public List<T> findAll() throws RuntimeException {

        List<T> list = null;
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("SELECT o FROM " + type.getName() + " o ORDER BY o.id");
        list = query.list();
        session.getTransaction().commit();

        return list;
    }

    @Override
    public T findByName(String name) throws RuntimeException {

        T ret = null;
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("SELECT o FROM " + type.getName() + " o WHERE o.name = :name");
        query.setParameter("name", name);
        if (query.list().size() > 0) {
            ret = (T) query.list().get(0);
        }
        session.getTransaction().commit();

        return ret;
    }

    @Override
    public T findById(ID id) throws RuntimeException {

        T ret = null;
        ret = findByIdNativeType(id);

        return ret;
    }


    @Override
    public ID save(T object) throws RuntimeException {

        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        ID id = (ID) session.save(object);
        session.getTransaction().commit();

        return id;
    }

    @Override
    public void update(T object) throws RuntimeException {

        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(object);
        session.getTransaction().commit();
    }

    @Override
    public void delete(T object) throws RuntimeException {

        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(object);
        session.getTransaction().commit();
    }

    @Override
    public void deleteById(ID id) throws RuntimeException {

        T object = findByIdNativeType(id);
        delete(object);
    }

    protected T findByIdNativeType(ID id) throws RuntimeException {

        T ret;
        if (id == null) {
            System.out.println("ID NOT FOUND");
            return null;
        }
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        ret = (T) session.get(type, id);
        session.getTransaction().commit();

        return ret;
    }
}
