package net.therap.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author nourin
 * @since 11/22/16
 */
public interface GenericDao<T, ID extends Serializable> {

    T findById(ID id) throws RuntimeException;

    T findByName(String name) throws RuntimeException;

    ID save(T object) throws RuntimeException;

    void update(T object) throws RuntimeException;

    List<T> findAll() throws RuntimeException;

    void delete(T object) throws RuntimeException;

    void deleteById(ID id) throws RuntimeException;
}
