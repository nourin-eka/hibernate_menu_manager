package net.therap.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/21/16
 */
@Entity
@Table(name = "item", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "name"})})
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "name", length = 45, nullable = true, unique = true)
    private String name;

    @ManyToMany(targetEntity = Type.class, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "item_type", joinColumns = {@JoinColumn(name = "item_id")},
            inverseJoinColumns = {@JoinColumn(name = "type_id")})
    private List<Type> types;

    public Item() {
    }

    public Item(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    public int hasType(Type type) {

        if(types == null) {
            types = new ArrayList<>();
        }

        for (int i = 0; i < getTypes().size(); i++) {

            Type t = getTypes().get(i);
            if (t.getName().toLowerCase().equals(type.getName().toLowerCase())) {

                return i;
            }
        }

        return -1;
    }

    public void addType(Type type) {

        if(hasType(type) == -1) {
            types.add(type);
        }
    }
}
