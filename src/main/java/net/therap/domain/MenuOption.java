package net.therap.domain;

/**
 * @author nourin
 * @since 11/14/16
 */
public enum MenuOption {

    ALL_MENU(1),
    BREAKFAST(2),
    LUNCH(3),
    UPDATE_BREAKFAST(4),
    UPDATE_LUNCH(5),
    EXIT(6),
    INVALID(7);

    private int value;

    MenuOption(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}