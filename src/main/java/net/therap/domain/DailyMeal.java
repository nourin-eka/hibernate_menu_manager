package net.therap.domain;

import javax.persistence.*;
import java.util.List;

/**
 * @author nourin
 * @since 11/22/16
 */
@Entity
@Table(name = "day", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "name"})})
public class DailyMeal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @ManyToMany(targetEntity = Item.class, fetch = FetchType.EAGER)
    @JoinTable(name = "daily_menu", joinColumns = {@JoinColumn(name = "day_id")},
            inverseJoinColumns = {@JoinColumn(name = "item_id")})
    private List<Item> items;

    public DailyMeal(String name, List<Item> items) {
        this.name = name;
        this.items = items;
    }

    public DailyMeal() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
