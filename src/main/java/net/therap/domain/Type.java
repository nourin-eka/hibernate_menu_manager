package net.therap.domain;

import javax.persistence.*;

/**
 * @author nourin
 * @since 11/21/16
 */
@Entity
@Table(name = "type", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "name"})})
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 11, unique = true)
    private Integer id;

    @Column(name = "name", length = 45, nullable = true, unique = true)
    private String name;

    public Type() {
    }

    public Type(String name) {
        this.name = name;
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }
}
